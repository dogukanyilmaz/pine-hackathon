### Cicek Sepeti Hackathon 

Sizlere sunulan program , herbir siparis koordinati bayi koordinatlari ile karsilastirip , 
en kisa yolu bulmasi uzerine kota limitleri dikkate alinarak hazirlanmistir. 
Ayni mesafedeki koordinatlar icin bir sonraki koordinatlari kontrol edip,
mesafeye gore karar verme uygulanmistir . 

Sonuc olarak , ister IDE console ‘ unda istersenizde web browserinizda sonuclari gorebilirsiniz.

## Başlatma
```
mvn spring-boot:run

```
İşletim sistemi windows olan bir bilgisayar ise otomatik olarak tarayıcınızda calışacaktır.
Eğer farklı bir işletim sistemi kullanıyorsanız, zip içerisindeki
```
 ...\Pine\hackathon\src\main\resources\static\index.html
 ```
 'i tarayıcınızda çalıştırmanız ve
bizlere gonderdiginiz koordinatlari barindiran excel dosyasini Dosya Seç butonu ile ekleyip,
Ekle butonuna tıkladıktan sonra sonuçlar Harita üzerinde ve aşağıda yazı olarak görebilirsiniz.  
Excel dosyaniz yoksa sizlere sundugumuz zip icinde bulabilirsiniz





# java(Spring Framework)
# javascript
# html
# css 

### Grup:Pine 

Dogukan YILMAZ
Ersin SEVINC

