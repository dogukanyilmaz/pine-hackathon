package com.pine.hackathon.Controller;

import com.pine.hackathon.DTO.MatchingDTO;
import com.pine.hackathon.DTO.ResponseDTO;
import com.pine.hackathon.Functions.Importer;
import com.pine.hackathon.Model.Company;
import com.pine.hackathon.Model.Order;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author doğukanyilmaz
 * Created by doğukanyilmaz on 3.03.2019
 * https://github.com/dogukanyilmaz95
 */
@RestController
@RequestMapping("again")
public class HackathonController {

    Order redOrder = new Order();
    Order greenOrder = new Order();
    Order blueOrder = new Order();
    MatchingDTO redMatch = new MatchingDTO();
    MatchingDTO greenMatch = new MatchingDTO();
    MatchingDTO blueMatch = new MatchingDTO();
    double red;
    double blue;
    double green;


    List<Order> orderList = new ArrayList<>();

    @CrossOrigin
    @PostMapping
    public ResponseEntity<?> hackathon(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook(multipartFile.getInputStream());
        orderList = Importer.importOrder(workbook.getSheetAt(0));
        final List<Company> companyList = Importer.importCompany(workbook.getSheetAt(1));

        redOrder.setLatitude(companyList.get(0).getLatitude());
        redOrder.setLongitude(companyList.get(0).getLongitude());
        greenOrder.setLatitude(companyList.get(1).getLatitude());
        greenOrder.setLongitude(companyList.get(1).getLongitude());
        blueOrder.setLatitude(companyList.get(2).getLatitude());
        blueOrder.setLongitude(companyList.get(2).getLongitude());

        redMatch.setCompany(companyList.get(0));
        greenMatch.setCompany(companyList.get(1));
        blueMatch.setCompany(companyList.get(2));

        calculateDistanceRed(redOrder, null);
        calculateDistanceGreen(greenOrder, null);
        calculateDistanceBlue(blueOrder, null);


        duplicateCtrl(redOrder, blueOrder, greenOrder);
        for (int i = 0; i < orderList.size(); i++) {
            calculateDistanceRed(redMatch.getOrderList().get(redMatch.getOrderList().size() - 1), null);
            calculateDistanceGreen(greenMatch.getOrderList().get(greenMatch.getOrderList().size() - 1), null);
            calculateDistanceBlue(blueMatch.getOrderList().get(blueMatch.getOrderList().size() - 1), null);

            duplicateCtrl(redOrder, blueOrder, greenOrder);
            redOrder = null;
            blueOrder = null;
            greenOrder = null;
            i = 0;
        }

        setBlueOrder(orderList.get(0));

        System.out.println("Kırmızı Şube Sipariş Atama sayısı = " + redMatch.getOrderList().size());
        System.out.println("Mavi Şube Sipariş Atama Sayısı = " + blueMatch.getOrderList().size());
        System.out.println("Yeşil Şube Sipariş Atama Sayısı = " + greenMatch.getOrderList().size());

        System.out.println("----------------------------------");


        green += Math.sqrt(Math.pow((greenMatch.getOrderList().get(0).getLongitude() - greenMatch.getOrderList().get(1).getLongitude()), 2) + Math.pow((greenMatch.getOrderList().get(0).getLatitude() - greenMatch.getOrderList().get(1).getLatitude()), 2));
        blue += Math.sqrt(Math.pow((blueMatch.getOrderList().get(0).getLongitude() - blueMatch.getOrderList().get(1).getLongitude()), 2) + Math.pow((blueMatch.getOrderList().get(0).getLatitude() - blueMatch.getOrderList().get(1).getLatitude()), 2));
        red += Math.sqrt(Math.pow((redMatch.getOrderList().get(0).getLongitude() - redMatch.getOrderList().get(1).getLongitude()), 2) + Math.pow((redMatch.getOrderList().get(0).getLatitude() - redMatch.getOrderList().get(1).getLatitude()), 2));

        System.out.println("Kırmızı Toplam Kuş uçuş mesafesi = " + red);
        System.out.println("Yeşil Toplam Kuş uçuş mesafesi = " + green);
        System.out.println("MAvi Toplam Kuş uçuş mesafesi  = " + blue);


        List<MatchingDTO> matching = new ArrayList<>();
        redMatch.getCompany().setCompanyName("Kırmızı");
        matching.add(redMatch);
        matching.add(greenMatch);
        matching.add(blueMatch);
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setMatchingDTOS(matching);
        responseDTO.setNumberOfBlue(blueMatch.getOrderList().size());
        responseDTO.setNumberOfGreen(greenMatch.getOrderList().size());
        responseDTO.setNumberOfRed(redMatch.getOrderList().size());
        responseDTO.setTotalDistanceOfBlue(blue);
        responseDTO.setTotalDistanceOfGreen(green);
        responseDTO.setTotalDistanceOfRed(red);
        resetAllData();

        return ResponseEntity.ok().body(responseDTO);
    }

    public double calculateDistanceRed(Order order, Order tempOrder) {
        double redCtrl = Math.sqrt(Math.pow((orderList.get(0).getLongitude() - order.getLongitude()), 2) + Math.pow((orderList.get(0).getLatitude() - order.getLatitude()), 2));

        for (int i = 0; i < orderList.size(); i++) {
            if (tempOrder != null && orderList.get(i).getOrderNo().equals(tempOrder.getOrderNo())) {
                i++;
            } else {
                double distanceRed = Math.sqrt(Math.pow((orderList.get(i).getLongitude() - order.getLongitude()), 2) + Math.pow((orderList.get(i).getLatitude() - order.getLatitude()), 2));
                if (distanceRed <= redCtrl) {
                    redCtrl = distanceRed;
                    redOrder = orderList.get(i);
                }
            }
        }
        return redCtrl;
    }

    public double calculateDistanceGreen(Order order, Order tempOrder) {
        double greenCtrl = Math.sqrt(Math.pow((orderList.get(0).getLongitude() - order.getLongitude()), 2) + Math.pow((orderList.get(0).getLatitude() - order.getLatitude()), 2));
        for (int i = 0; i < orderList.size(); i++) {
            if (tempOrder != null && orderList.get(i).getOrderNo().equals(tempOrder.getOrderNo())) {
                i++;
            } else {
                double distanceGreen = Math.sqrt(Math.pow((orderList.get(i).getLongitude() - order.getLongitude()), 2) + Math.pow((orderList.get(i).getLatitude() - order.getLatitude()), 2));
                if (distanceGreen <= greenCtrl) {
                    greenCtrl = distanceGreen;
                    greenOrder = orderList.get(i);
                }
            }
        }
        return greenCtrl;
    }

    public double calculateDistanceBlue(Order order, Order tempOrder) {
        double blueCtrl = Math.sqrt(Math.pow((orderList.get(0).getLongitude() - order.getLongitude()), 2) + Math.pow((orderList.get(0).getLatitude() - order.getLatitude()), 2));

        for (int i = 0; i < orderList.size(); i++) {
            if (tempOrder != null && orderList.get(i).getOrderNo().equals(tempOrder.getOrderNo())) {
                i++;
            } else {
                double distanceBlue = Math.sqrt(Math.pow((orderList.get(i).getLongitude() - order.getLongitude()), 2) + Math.pow((orderList.get(i).getLatitude() - order.getLatitude()), 2));
                if (distanceBlue <= blueCtrl) {
                    blueCtrl = distanceBlue;
                    blueOrder = orderList.get(i);
                }
            }
        }
        return blueCtrl;
    }

    public void duplicateCtrl(Order redOrderM, Order greenOrderM, Order blueOrderM) {
        if (redOrderM.getOrderNo().equals(greenOrderM.getOrderNo()) && redOrderM.getOrderNo().equals(blueOrderM.getOrderNo())) {

            dublicateAllOrder(redOrderM, greenOrderM, blueOrderM);

        } else {
            if (redOrderM.getOrderNo().equals(greenOrderM.getOrderNo())) {

                setBlueOrder(blueOrderM);

                double greenDistance = calculateDistanceGreen(greenMatch.getOrderList().get(greenMatch.getOrderList().size() - 1), greenOrderM);
                double redDistance = calculateDistanceRed(redMatch.getOrderList().get(redMatch.getOrderList().size() - 1), redOrderM);

                if (greenOrder.getOrderNo().equals(redOrder.getOrderNo())) {
                    if (greenDistance < redDistance) {
                        setGreenOrder(greenOrder);
                        setRedOrder(redOrderM);
                    } else {
                        setGreenOrder(greenOrderM);
                        setRedOrder(redOrder);
                    }
                } else {
                    if (greenDistance < redDistance) {
                        setGreenOrder(greenOrder);
                        setRedOrder(redOrderM);
                    } else {
                        setGreenOrder(greenOrderM);
                        setRedOrder(redOrder);
                    }
                }
            } else if (redOrderM.getOrderNo().equals(blueOrderM.getOrderNo())) {
                setGreenOrder(greenOrderM);

                double blueDistance = calculateDistanceBlue(blueMatch.getOrderList().get(blueMatch.getOrderList().size() - 1), blueOrderM);
                double redDistance = calculateDistanceRed(redMatch.getOrderList().get(redMatch.getOrderList().size() - 1), redOrderM);

                if (blueOrder.getOrderNo().equals(redOrder.getOrderNo())) {
                    if (blueDistance < redDistance) {
                        setBlueOrder(blueOrder);
                        setRedOrder(redOrderM);
                    } else {
                        setBlueOrder(blueOrderM);
                        setRedOrder(redOrder);
                    }
                } else {
                    if (blueDistance < redDistance) {
                        setBlueOrder(blueOrder);
                        setRedOrder(redOrderM);
                    } else {
                        setBlueOrder(blueOrderM);
                        setRedOrder(redOrder);
                    }
                }
            } else if (blueOrderM.getOrderNo().equals(greenOrderM.getOrderNo())) {
                setRedOrder(redOrderM);


                double greenDistance = calculateDistanceGreen(greenMatch.getOrderList().get(greenMatch.getOrderList().size() - 1), greenOrderM);
                double blueDistance = calculateDistanceBlue(blueMatch.getOrderList().get(blueMatch.getOrderList().size() - 1), blueOrderM);

                if (greenOrder.getOrderNo().equals(blueOrder.getOrderNo())) {
                    if (greenDistance < blueDistance) {
                        setGreenOrder(greenOrder);
                        setBlueOrder(blueOrderM);
                    } else {
                        setGreenOrder(greenOrderM);
                        setBlueOrder(blueOrder);
                    }
                } else {
                    if (greenDistance < blueDistance) {
                        setGreenOrder(greenOrder);
                        setBlueOrder(blueOrderM);
                    } else {
                        setGreenOrder(greenOrderM);
                        setBlueOrder(blueOrder);
                    }
                }

            } else {

                setRedOrder(redOrderM);
                setGreenOrder(greenOrderM);
                setBlueOrder(blueOrderM);
            }

        }

    }

    public void dublicateAllOrder(Order redOrderM, Order greenOrderM, Order blueOrderM) {
        double greenCtrl = calculateDistanceGreen(greenMatch.getOrderList().get(greenMatch.getOrderList().size() - 1), null);
        double redCtrl = calculateDistanceRed(redMatch.getOrderList().get(redMatch.getOrderList().size() - 1), null);
        double blueCtrl = calculateDistanceBlue(blueMatch.getOrderList().get(blueMatch.getOrderList().size() - 1), null);

        if (greenCtrl < redCtrl && greenCtrl < blueCtrl) {

            setGreenOrder(greenOrderM);

            double redDistance = calculateDistanceRed(redMatch.getOrderList().get(redMatch.getOrderList().size() - 1), null);
            double blueDistance = calculateDistanceBlue(blueMatch.getOrderList().get(blueMatch.getOrderList().size() - 1), null);

            if (blueOrder.getOrderNo().equals(redOrder.getOrderNo())) {
                if (blueDistance < redDistance) {

                    setBlueOrder(blueOrder);
                    setRedOrder(redOrderM);
                } else {
                    setBlueOrder(blueOrderM);
                    setRedOrder(redOrder);
                }
            } else {
                if (blueDistance < redDistance) {
                    setBlueOrder(blueOrder);
                    setRedOrder(redOrderM);
                } else {
                    setBlueOrder(blueOrderM);
                    setRedOrder(redOrder);
                }
            }

        } else if (blueCtrl < greenCtrl && blueCtrl < redCtrl) {
            setBlueOrder(blueOrderM);

            double greenDistance = calculateDistanceGreen(greenMatch.getOrderList().get(greenMatch.getOrderList().size() - 1), greenOrderM);
            double redDistance = calculateDistanceRed(redMatch.getOrderList().get(redMatch.getOrderList().size() - 1), redOrderM);

            if (greenOrder.getOrderNo().equals(redOrder.getOrderNo())) {
                if (greenDistance < redDistance) {
                    setGreenOrder(greenOrder);
                    setRedOrder(redOrderM);
                } else {
                    setGreenOrder(greenOrderM);
                    setRedOrder(redOrder);
                }
            } else {
                if (greenDistance < redDistance) {
                    setGreenOrder(greenOrder);
                    setRedOrder(redOrderM);
                } else {
                    setGreenOrder(greenOrderM);
                    setRedOrder(redOrder);
                }
            }
        } else {
            setRedOrder(redOrderM);

            double greenDistance = calculateDistanceGreen(greenMatch.getOrderList().get(greenMatch.getOrderList().size() - 1), greenOrderM);
            double blueDistance = calculateDistanceBlue(blueMatch.getOrderList().get(blueMatch.getOrderList().size() - 1), blueOrderM);

            if (greenOrder.getOrderNo().equals(blueOrder.getOrderNo())) {
                if (greenDistance < blueDistance) {
                    setGreenOrder(greenOrder);
                    setBlueOrder(blueOrderM);
                } else {
                    setGreenOrder(greenOrderM);
                    setBlueOrder(blueOrder);
                }
            } else {
                if (greenDistance < blueDistance) {
                    setGreenOrder(greenOrder);
                    setBlueOrder(blueOrderM);
                } else {
                    setGreenOrder(greenOrderM);
                    setBlueOrder(blueOrder);
                }
            }

        }
    }

    public void setBlueOrder(Order order) {

        if (blueMatch.getOrderList().size() == 0) {
            if (blueMatch.getOrderList().size() != 0) {
                double blue1 = Math.sqrt(Math.pow((order.getLongitude() - blueMatch.getOrderList().get(blueMatch.getOrderList().size() - 1).getLongitude()), 2) + Math.pow((order.getLatitude() - blueMatch.getOrderList().get(blueMatch.getOrderList().size() - 1).getLatitude()), 2));
                blue = blue + blue1;
            }

            blueMatch.getOrderList().add(order);
            orderList.remove(order);

        }
        if (redMatch.getOrderList().size() == 30 && greenMatch.getOrderList().size() == 35) {
            if (blueMatch.getOrderList().size() != 0) {
                double blue1 = Math.sqrt(Math.pow((order.getLongitude() - blueMatch.getOrderList().get(blueMatch.getOrderList().size() - 1).getLongitude()), 2) + Math.pow((order.getLatitude() - blueMatch.getOrderList().get(blueMatch.getOrderList().size() - 1).getLatitude()), 2));
                blue = blue + blue1;
            }
            blueMatch.getOrderList().add(order);
            orderList.remove(order);


        }

        for (int j = 0; j < orderList.size(); j++) {
            if (blueMatch.getOrderList().size() != 0) {
                double blueNearDistance = Math.sqrt(Math.pow((orderList.get(j).getLongitude() - blueMatch.getOrderList().get(blueMatch.getOrderList().size() - 1).getLongitude()), 2) + Math.pow((orderList.get(j).getLatitude() - blueMatch.getOrderList().get(blueMatch.getOrderList().size() - 1).getLatitude()), 2));
                if (blueNearDistance == 0.0 && redMatch.getOrderList().size() == 30 && greenMatch.getOrderList().size() == 35) {
                    double blue1 = Math.sqrt(Math.pow((orderList.get(j).getLongitude() - blueMatch.getOrderList().get(blueMatch.getOrderList().size() - 1).getLongitude()), 2) + Math.pow((orderList.get(j).getLatitude() - blueMatch.getOrderList().get(blueMatch.getOrderList().size() - 1).getLatitude()), 2));
                    blue = blue + blue1;
                    blueMatch.getOrderList().add(orderList.get(j));
                    orderList.remove(orderList.get(j));


                }
            }

        }
    }

    public void setRedOrder(Order order) {
        if (redMatch.getOrderList().size() < 30) {
            if (redMatch.getOrderList().size() != 0) {
                double red1 = Math.sqrt(Math.pow((order.getLongitude() - redMatch.getOrderList().get(redMatch.getOrderList().size() - 1).getLongitude()), 2) + Math.pow((order.getLatitude() - redMatch.getOrderList().get(redMatch.getOrderList().size() - 1).getLatitude()), 2));
                red = red1 + red;
            }
            redMatch.getOrderList().add(order);
            orderList.remove(order);

        }

        for (int j = 0; j < orderList.size(); j++) {
            double redNearDistance = Math.sqrt(Math.pow((orderList.get(j).getLongitude() - redMatch.getOrderList().get(redMatch.getOrderList().size() - 1).getLongitude()), 2) + Math.pow((orderList.get(j).getLatitude() - redMatch.getOrderList().get(redMatch.getOrderList().size() - 1).getLatitude()), 2));
            if (redNearDistance == 0.0 && redMatch.getOrderList().size() < 30) {
                if (redMatch.getOrderList().size() != 0) {
                    double red1 = Math.sqrt(Math.pow((order.getLongitude() - redMatch.getOrderList().get(redMatch.getOrderList().size() - 1).getLongitude()), 2) + Math.pow((order.getLatitude() - redMatch.getOrderList().get(redMatch.getOrderList().size() - 1).getLatitude()), 2));
                    red = red1 + red;
                }
                redMatch.getOrderList().add(orderList.get(j));
                orderList.remove(orderList.get(j));
            }
        }
    }

    public void setGreenOrder(Order order) {
        if (greenMatch.getOrderList().size() < 35) {
            if (greenMatch.getOrderList().size() != 0) {
                double green1 = Math.sqrt(Math.pow((order.getLongitude() - greenMatch.getOrderList().get(greenMatch.getOrderList().size() - 1).getLongitude()), 2) + Math.pow((order.getLatitude() - greenMatch.getOrderList().get(greenMatch.getOrderList().size() - 1).getLatitude()), 2));
                green = green + green1;
            }
            greenMatch.getOrderList().add(order);
            orderList.remove(order);


        }

        for (int j = 0; j < orderList.size(); j++) {
            double greenNearDistance = Math.sqrt(Math.pow((orderList.get(j).getLongitude() - greenMatch.getOrderList().get(greenMatch.getOrderList().size() - 1).getLongitude()), 2) + Math.pow((orderList.get(j).getLatitude() - greenMatch.getOrderList().get(greenMatch.getOrderList().size() - 1).getLatitude()), 2));
            if (greenNearDistance == 0.0 && greenMatch.getOrderList().size() < 35) {
                if (greenMatch.getOrderList().size() != 0) {
                    double green1 = Math.sqrt(Math.pow((order.getLongitude() - greenMatch.getOrderList().get(greenMatch.getOrderList().size() - 1).getLongitude()), 2) + Math.pow((order.getLatitude() - greenMatch.getOrderList().get(greenMatch.getOrderList().size() - 1).getLatitude()), 2));
                    green = green + green1;
                }
                greenMatch.getOrderList().add(orderList.get(j));
                orderList.remove(orderList.get(j));
            }
        }
    }

    public void resetAllData(){
        redOrder = new Order();
        greenOrder = new Order();
        blueOrder = new Order();
        redMatch = new MatchingDTO();
        greenMatch = new MatchingDTO();
        blueMatch = new MatchingDTO();
        blue = 0.0;
        red = 0.0;
        green = 0.0;
    }

}
