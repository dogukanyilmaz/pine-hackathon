package com.pine.hackathon.DTO;

import com.pine.hackathon.Model.Company;
import com.pine.hackathon.Model.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * @author doğukanyilmaz
 * Created by doğukanyilmaz on 1.03.2019
 * https://github.com/dogukanyilmaz95
 */
public class MatchingDTO {
    List<Order> orderList = new ArrayList<>();
    Company company;

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
