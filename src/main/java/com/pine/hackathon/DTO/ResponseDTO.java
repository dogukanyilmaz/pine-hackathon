package com.pine.hackathon.DTO;

import com.pine.hackathon.Model.Company;
import com.pine.hackathon.Model.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * @author doğukanyilmaz
 * Created by doğukanyilmaz on 3.03.2019
 * https://github.com/dogukanyilmaz95
 */
public class ResponseDTO {
    List<MatchingDTO> matchingDTOS;

    int numberOfRed;
    int numberOfGreen;
    int numberOfBlue;

    double totalDistanceOfRed;
    double totalDistanceOfGreen;
    double totalDistanceOfBlue;


    public List<MatchingDTO> getMatchingDTOS() {
        return matchingDTOS;
    }

    public void setMatchingDTOS(List<MatchingDTO> matchingDTOS) {
        this.matchingDTOS = matchingDTOS;
    }

    public int getNumberOfRed() {
        return numberOfRed;
    }

    public void setNumberOfRed(int numberOfRed) {
        this.numberOfRed = numberOfRed;
    }

    public int getNumberOfGreen() {
        return numberOfGreen;
    }

    public void setNumberOfGreen(int numberOfGreen) {
        this.numberOfGreen = numberOfGreen;
    }

    public int getNumberOfBlue() {
        return numberOfBlue;
    }

    public void setNumberOfBlue(int numberOfBlue) {
        this.numberOfBlue = numberOfBlue;
    }

    public double getTotalDistanceOfRed() {
        return totalDistanceOfRed;
    }

    public void setTotalDistanceOfRed(double totalDistanceOfRed) {
        this.totalDistanceOfRed = totalDistanceOfRed;
    }

    public double getTotalDistanceOfGreen() {
        return totalDistanceOfGreen;
    }

    public void setTotalDistanceOfGreen(double totalDistanceOfGreen) {
        this.totalDistanceOfGreen = totalDistanceOfGreen;
    }

    public double getTotalDistanceOfBlue() {
        return totalDistanceOfBlue;
    }

    public void setTotalDistanceOfBlue(double totalDistanceOfBlue) {
        this.totalDistanceOfBlue = totalDistanceOfBlue;
    }
}
