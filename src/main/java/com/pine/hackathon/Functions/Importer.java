package com.pine.hackathon.Functions;

import com.pine.hackathon.Model.Company;
import com.pine.hackathon.Model.Order;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.ArrayList;
import java.util.List;

/**
 * @author doğukanyilmaz
 * Created by doğukanyilmaz on 27.02.2019
 * https://github.com/dogukanyilmaz95
 */
public class Importer {

    public static List<Order> importOrder(XSSFSheet worksheet) {
        List<Order> orderList = new ArrayList<Order>();
        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            Order order = new Order();

            XSSFRow row = worksheet.getRow(i);
            order.setOrderNo(row.getCell(0).getStringCellValue());
            order.setLatitude(Double.parseDouble(row.getCell(1).getStringCellValue()));
            order.setLongitude(Double.parseDouble(row.getCell(2).getStringCellValue()));
            orderList.add(order);
        }

        return orderList;
    }

    public static List<Company> importCompany(XSSFSheet worksheet) {
        List<Company> companyList = new ArrayList<Company>();

        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            Company company = new Company();

            XSSFRow row = worksheet.getRow(i);
            company.setCompanyName(row.getCell(0).getStringCellValue());
            company.setLatitude(Double.parseDouble(row.getCell(1).getStringCellValue()));
            company.setLongitude(Double.parseDouble(row.getCell(2).getStringCellValue()));
            companyList.add(company);
        }

        return companyList;
    }
}
