package com.pine.hackathon.Model;

/**
 * @author doğukanyilmaz
 * Created by doğukanyilmaz on 27.02.2019
 * https://github.com/dogukanyilmaz95
 */
public class Company {
    private Double Longitude;
    private Double Latitude;
    private String CompanyName;

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }
}
