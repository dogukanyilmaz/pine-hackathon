package com.pine.hackathon.Model;

/**
 * @author doğukanyilmaz
 * Created by doğukanyilmaz on 27.02.2019
 * https://github.com/dogukanyilmaz95
 */
public class Order {
    private Double Longitude;
    private Double Latitude;
    private String OrderNo;
    private String CompanyColor;

    public String getCompanyColor() {
        return CompanyColor;
    }

    public void setCompanyColor(String companyColor) {
        CompanyColor = companyColor;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }
}
