var globalContext;
var allMakers = [];
var magaza = [];
var markerObj;
var initMap = (function () {
    function initMap() {
        globalContext = this;
    }


    initMap.prototype.Marker = function () {
        $("#btnSubmit").click(function (event) {
            console.log("DOCUMENT READY");
            // var form = ;

            // Create an FormData object
            var formData = new FormData();
            formData.append("file", $('#file1')[0].files[0], "siparis");

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "http://localhost:8080/again",
                processData: false,  // Important!
                contentType: false,
                cache: false,
                data: formData,
                success: function (res) {
                    console.log("getAll Response = " + res);




                    document.getElementById('redCount').innerHTML= "Kırmızı Kota : " + res.numberOfRed;
                    document.getElementById('greenCount').innerHTML= "Yeşil Kota: " + res.numberOfGreen;
                    document.getElementById('blueCount').innerHTML= "Mavi Kota: " + res.numberOfBlue;

                    document.getElementById('redDist').innerHTML= "Kırmızı Mesafe: " + res.totalDistanceOfBlue;
                    document.getElementById('greenDist').innerHTML= "Yeşil Mesafe: " + res.totalDistanceOfGreen;
                    document.getElementById('blueDist').innerHTML= "Mavi Mesafe: " + res.totalDistanceOfRed;
                    res.matchingDTOS.forEach(function (res1) {

                        markerObj = {
                            position: new google.maps.LatLng(res1.company.latitude, res1.company.longitude),
                            type: res1.company.companyName,
                        };
                        magaza.push(markerObj);

                        res1.orderList.forEach(function (res2) {
                            markerObj = {
                                position: new google.maps.LatLng(res2.latitude, res2.longitude),
                                type: res1.company.companyName,
                            };
                            allMakers.push(markerObj);

                        })

                    });
                    globalContext.CreateMap();

                }
            });
        });
    };

    initMap.prototype.CreateMap = function () {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: new google.maps.LatLng(41.049997, 29.026108),
            mapTypeId: 'roadmap'
        });

        var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';

        var icons = {
            Kırmızı: {
                icon: 'icon/redPin.png'
            },
            Yeşil: {
                icon: 'icon/greenPin.png'
            },
            Mavi: {
                icon: 'icon/bluePin.png'
            }
        };

        var shopİcons = {

            Kırmızı: {
                icon: 'icon/redCircle.png'
            },
            Yeşil: {
                icon: 'icon/greenCircle.png'
            },
            Mavi: {
                icon: 'icon/blueCircle.png'
            }
        };


        allMakers.forEach(function (feature) {
            var marker = new google.maps.Marker({
                position: feature.position,
                icon: icons[feature.type].icon,
                id: feature.id,
                map: map,
            });


            marker.addListener('click', function () {

                map.setCenter(marker.getPosition());
                infoWindow.open(map, this);
            });

        })

        magaza.forEach(function (shop) {

            var shopMarker = new google.maps.Marker({

                position: shop.position,
                icon: shopİcons[shop.type].icon,
                map:map,

            });

        })


    };

    return initMap;
})();